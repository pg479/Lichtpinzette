import pandas as pd
import numpy as np


MOTOR_VELOCITY = 10 # µm/s
MOTOR_VELOCITY_ERR = 1 # µm/s

DRIFT_DATA = r'massstab/drift.hdf5'
MOVING_DATA = r'massstab/moving.hdf5'


# lineare Fitfunktion durch Ursprung
def func(x, a):
    return a * x



### HINTERGRUNDBEWEGUNG ERMITTELN ###

# Datei einlesen
dataframe_drift = pd.read_hdf(DRIFT_DATA)
df_drift = dataframe_drift.set_index('particle')
particles_drift = np.unique(dataframe_drift['particle'])
frames_drift = np.max(df_drift['frame'])+1

# Durchschnittsgeschwindigkeiten messen
x_bg_velocities_drift = [] # [pxl]
y_bg_velocities_drift = [] # [pxl]

for p in particles_drift:
    particle_data = df_drift.loc[p]
    x = np.array(particle_data['x'])
    y = np.array(particle_data['y'])

    x_bg_velocities_drift.append((x[-1] - x[0]) / len(x))
    y_bg_velocities_drift.append((y[-1] - y[0]) / len(y))

# Hintergrundgeschwindigkeit
x_bg_velocity_drift = np.average(x_bg_velocities_drift) # [pxl/frame]
y_bg_velocity_drift = np.average(y_bg_velocities_drift) # [pxl/frame]

x_bg_velocity_drift_err = np.sqrt(np.var(x_bg_velocities_drift)) # [pxl/frame]
y_bg_velocity_drift_err = np.sqrt(np.var(y_bg_velocities_drift)) # [pxl/frame]

#######################################



### BEWEGUNG DURCH MOTOR MESSEN ###

# Datei einlesen
dataframe_moving = pd.read_hdf(MOVING_DATA)
df_moving = dataframe_moving.set_index('particle')
particles_moving = np.unique(dataframe_moving['particle'])
frames_moving = np.max(df_moving['frame'])+1

# Durchschnittsgeschwindigkeiten messen
x_bg_velocities_moving = []
y_bg_velocities_moving = []
amount_moving = 0

for p in particles_moving:
    particle_data = df_moving.loc[p]
    x = np.array(particle_data['x'])
    y = np.array(particle_data['y'])

    x_bg_velocities_moving.append((x[-1] - x[0]) / len(x))
    y_bg_velocities_moving.append((y[-1] - y[0]) / len(y))
    amount_moving += 1

x_bg_velocity_moving = np.average(x_bg_velocities_moving) # [pxl/frame]
y_bg_velocity_moving = np.average(y_bg_velocities_moving) # [pxl/frame]

x_bg_velocity_moving_err = np.sqrt(np.var(x_bg_velocities_moving)) # [pxl/frame]
y_bg_velocity_moving_err = np.sqrt(np.var(y_bg_velocities_moving)) # [pxl/frame]

# Hintergrundgeschwindigkeit abziehen
x_bg_velocity_moving -= x_bg_velocity_drift # [pxl/frame]
y_bg_velocity_moving -= y_bg_velocity_drift # [pxl/frame]

x_bg_velocity_moving_err = np.sqrt(x_bg_velocity_moving_err**2 + x_bg_velocity_drift_err**2) # [pxl/frame]
y_bg_velocity_moving_err = np.sqrt(y_bg_velocity_moving_err**2 + y_bg_velocity_drift_err**2) # [pxl/frame]

######################################



### ERGEBNISSE ###

pxlvelocity = np.sqrt(x_bg_velocity_moving**2 + y_bg_velocity_moving**2) * 30  # [pxl/s]
pxlvelocity_err = np.sqrt(  (2*x_bg_velocity_moving / (x_bg_velocity_moving**2 + y_bg_velocity_moving**2) * x_bg_velocity_moving_err)**2
                          + (2*y_bg_velocity_moving / (x_bg_velocity_moving**2 + y_bg_velocity_moving**2) * y_bg_velocity_moving_err)**2) * 30 # [pxl/s]

mic_per_pxl = MOTOR_VELOCITY / pxlvelocity # [µm/pxl]
mic_per_pxl_err = np.sqrt((MOTOR_VELOCITY_ERR / pxlvelocity)**2 + (-MOTOR_VELOCITY / pxlvelocity**2 * pxlvelocity_err)**2) # [µm/pxl]
pxl_per_mic = pxlvelocity / MOTOR_VELOCITY # [pxl/µm]
pxl_per_mic_err = np.sqrt((pxlvelocity_err / MOTOR_VELOCITY)**2 + (-pxlvelocity / MOTOR_VELOCITY**2 * MOTOR_VELOCITY_ERR)**2) # [pxl/µm]


print()
print(f'Partikel im unbewegten Video betrachtet: {len(particles_drift)}')
print(f'Länge des unbewegten Videos: {frames_drift} Frames / {frames_drift/30}s')
print(f'Hintergrundgeschwindigkeit: x: {x_bg_velocity_drift} pxl/s y: {y_bg_velocity_drift} pxl/s')
print()
print(f'Partikel im bewegten Video betrachtet: {len(particles_moving)}')
print(f'Länge des bewegten Videos: {frames_moving} Frames / {frames_moving/30}s')
print()
print('ERGEBNISSE')
print(f'x-Geschwindigkeit: {x_bg_velocity_moving * 30} ± {x_bg_velocity_moving_err * 30} pxl/s')
print(f'y-Geschwindigkeit: {y_bg_velocity_moving * 30} ± {y_bg_velocity_moving_err * 30} pxl/s')
print(f'Gesamt: {pxlvelocity} ± {pxlvelocity_err} pxl/s')
print(f'Also: {mic_per_pxl} ± {mic_per_pxl_err} µm/pxl')
print(f'Bzw.: {pxl_per_mic} ± {pxl_per_mic_err} pxl/µm')
print()