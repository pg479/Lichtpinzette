import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

### KONSTANTEN ###

BOLTZMANN = 1.380649e-23 # J/K = Nm/K
µ = 1e-6

R = 2.06/2 # µm
T = 25 + 273.15 # K

RERR = 0.05 # µm
TERR = 5 # K

LASER = (930,117)
MICROMETERS_PER_PIXEL = 0.06105198218175463
MICROMETERS_PER_PIXEL_ERR = 0.006359534593926663


#DATA = r'massstab/drift.hdf5'
#DATA = r'brownsche_bewegung/bb.hdf5'
#DATA = r'brownsche_bewegung/bb1.hdf5'
DATA = r'brownsche_bewegung/bb2.hdf5'
#DATA = r'Pinzette_funk_brown/pinzette.hdf5'


MIN_VELOCITY = 0.01 # µm²/s
MAX_Y_START = 5 # µm²

SHOW_BG_MOVEMENT = True

CORRECT_FOR_3D = False
DIMENSION_FACTOR = 4
if CORRECT_FOR_3D: DIMENSION_FACTOR = 6

##################



# Datei einlesen
dataframe = pd.read_hdf(DATA)#)#r'C:\Users\Lenna\Downloads\New folder (2)\DSC_9102_MOV_AdobeExpress.hdf5')
df = dataframe.set_index('particle')
particles = np.unique(dataframe['particle'])
frames = np.max(df['frame'])+1

#-- ZUM FINDEN DER GEFANGENEN PARTIKEL --#
# df = df[np.abs(df['x'] - LASER[0]) < 100]
# df = df[np.abs(df['y'] - LASER[1]) < 100]
# print(df)
# exit()
#--                                    --#

# Funktion zum printen von Informationen bei Klick auf Diagramm
def on_pick(event):
    artist = event.artist
    xmouse, ymouse = event.mouseevent.xdata, event.mouseevent.ydata
    x, y = artist.get_xdata(), artist.get_ydata()
    ind = event.ind
    print('Artist picked:', event.artist)
    print('{} vertices picked'.format(len(ind)))
    print('Pick between vertices {} and {}'.format(min(ind), max(ind)+1))
    print('x, y of mouse: {:.2f},{:.2f}'.format(xmouse, ymouse))
    print('Data point:', x[ind[0]], y[ind[0]])

# lineare Fitfunktion durch Ursprung
def func(x, a):
    return a * x

# lineare Fitfunktion mit y-Achsenabschnitt
def func2(x, a, b):
    return a * x + b



### BESTIMMUNG DER DURCHSCHNITTLICHEN BEWEGUNG ###
x_bg_velocity = np.zeros(frames)
y_bg_velocity = np.zeros(frames)
amount = np.zeros(frames) # Anzahl Partikel in einem Frame

to_ignore = []

for p in particles:
    particle_data = df.loc[p]
    x = np.array(particle_data['x'])
    y = np.array(particle_data['y'])

    #-- Bewegt sich der Partikel überhaupt? --#
    sqrdistance = (((x - x[0])**2 + (y - y[0])**2)) * MICROMETERS_PER_PIXEL**2
    t = np.linspace(0, len(sqrdistance)/30, len(sqrdistance))

    popt, pcov = curve_fit(func2, t, sqrdistance)
    if not MIN_VELOCITY < popt[0] or abs(popt[1]) > MAX_Y_START: to_ignore.append(p); continue # Weniger als MIN_VELOCITY => Partikel ist unbeweglich, y-Achsenabschnitt zu hoch => Partikel wird später unbeweglich
    #--                                     --#

    x_bg_velocity[particle_data['frame'][:-1]] += x[1:] - x[:-1]
    y_bg_velocity[particle_data['frame'][:-1]] += y[1:] - y[:-1]
    amount[particle_data['frame'][:-1]] += 1

# Anpassung für Partikelanzahl
x_bg_velocity /= amount
y_bg_velocity /= amount
x_bg_velocity[amount < 1] = 0
y_bg_velocity[amount < 1] = 0

# rolling average
x_bg_velocity = np.convolve(x_bg_velocity, np.ones(600)/600, mode='same')
y_bg_velocity = np.convolve(y_bg_velocity, np.ones(600)/600, mode='same')
x_bg_velocity = np.convolve(x_bg_velocity, np.ones(600)/600, mode='same')
y_bg_velocity = np.convolve(y_bg_velocity, np.ones(600)/600, mode='same')
x_bg_velocity = np.convolve(x_bg_velocity, np.ones(600)/600, mode='same')
y_bg_velocity = np.convolve(y_bg_velocity, np.ones(600)/600, mode='same')

if SHOW_BG_MOVEMENT:
    plt.title('Hintergrundgeschwindigkeit')
    t = np.arange(len(x_bg_velocity))/30
    plt.plot(t, x_bg_velocity * MICROMETERS_PER_PIXEL*1000*30, label='Durchschnittliche Geschwindigkeit in x-Richtung')
    plt.plot(t, y_bg_velocity * MICROMETERS_PER_PIXEL*1000*30, label='Durchschnittliche Geschwindigkeit in y-Richtung')
    plt.xlabel('Zeit [s]')
    plt.ylabel('Geschwindigkeit [nm/s]')
    plt.legend()
    plt.show()

x_bg_position = np.zeros_like(x_bg_velocity)
y_bg_position = np.zeros_like(y_bg_velocity)
# integration
x_bg_position[1:] = np.convolve(x_bg_velocity, np.ones_like(x_bg_velocity), mode='full')[:frames-1]
y_bg_position[1:] = np.convolve(y_bg_velocity, np.ones_like(y_bg_velocity), mode='full')[:frames-1]

if SHOW_BG_MOVEMENT:
    plt.title('Hintergrundtrajektorie')
    plt.plot(x_bg_position * MICROMETERS_PER_PIXEL, y_bg_position * MICROMETERS_PER_PIXEL, color='black')
    plt.xlabel('Verschiebung in x-Richtung [µm]')
    plt.ylabel('Verschiebung in y-Richtung [µm]')
    plt.gca().invert_yaxis()
    ax = plt.gca()
    ax.set_aspect('equal', adjustable='box')
    plt.show()

################################################



### BESTIMMUNG DER KORRIGIERTEN ZURÜCKGELEGTEN STRECKEN ###

fig, ax = plt.subplots()
fig.canvas.callbacks.connect('pick_event', on_pick)

Ms = []
errs = []
for p in particles:
    if p in to_ignore: continue

    particle_data = df.loc[p]
    x = np.array(particle_data['x'])
    y = np.array(particle_data['y'])

    # Korrektur mit Hintergrundtrajektorie
    x -= x_bg_position[particle_data['frame']] - x_bg_position[particle_data['frame']][0]
    y -= y_bg_position[particle_data['frame']] - y_bg_position[particle_data['frame']][0]

    # Quadratische Wegstrecke & Zeit
    sqrdistance = (((x - x[0])**2 + (y - y[0])**2)) * MICROMETERS_PER_PIXEL**2
    t = np.linspace(0, len(sqrdistance)/30, len(sqrdistance))

    # Fit
    popt, pcov = curve_fit(func, t, sqrdistance)

    Ms.append(popt[0])
    errs.append(np.sqrt(np.diag(pcov)))
    
    ax.plot(t, sqrdistance, label=f'{p}', picker=10)

##########################################################



# Umrechnung der Quadratischen Wegstreckensteigung m [µm²/s] in die Viskosität η [Ns/m²]
def vis(m):
    return DIMENSION_FACTOR*BOLTZMANN*T/(6*np.pi * R*µ * m*µ**2)

# Berechnung des Fehlers der Viskosität η [Ns/m²] aus der Quadratischen Wegstreckensteigung m [µm²/s] und ihrem Fehler e [µm²/s]
def vis_err(m, e):
    return np.sqrt(                                                                                                 # sqrt(
              (DIMENSION_FACTOR*BOLTZMANN / (6*np.pi * R*µ      * m*µ**2)                         * TERR  )**2                       #  (∂η/∂T * ∆T)²
           +(DIMENSION_FACTOR*BOLTZMANN*T / (6*np.pi * (R*µ)**2 * m*µ**2)                         * RERR*µ)**2                       # +(∂η/∂R * ∆R)²
          +(-DIMENSION_FACTOR*BOLTZMANN*T / (6*np.pi * R*µ      * (m*µ**2)**2)                    * e*µ**2)**2                       # +(∂η/∂M * ∆M)²
        +(-2*DIMENSION_FACTOR*BOLTZMANN*T / (6*np.pi * R*µ      * m*µ**2 * MICROMETERS_PER_PIXEL) * MICROMETERS_PER_PIXEL_ERR)**2)   # +(∂η/∂MICROMETERS_PER_PIXEL * ∆MICROMETERS_PER_PIXEL)² )


# Umrechnung in Viskosität
Ms = np.array(Ms)
if CORRECT_FOR_3D:
    Ms /= (np.pi/4)**2 # Erwartete zurückgelegte Strecke

M = np.average(Ms)
M_err = np.sqrt(np.var(Ms)/len(Ms))

eta = vis(M)
err = vis_err(M, M_err)

# Ergebnisse
print()
print(f'Daten von: {DATA}')
print(f'Partikel insgesamt betrachtet: {len(Ms)}')
print(f'Länge des Videos: {frames} Frames / {frames/30}s')
print(f'Viskosität [Ns/m²]: {eta} ± {err}')
print()

plt.ylabel('Quadratische Verschiebung [µm²]')
plt.xlabel('Zeit [s]')
plt.title('Quadratische Verschiebung aller Partikel (interaktiv)')

plt.show()

# Gesamtergebnis: 0.0010157326319665924 ± 0.000105196049151901 Ns/m²