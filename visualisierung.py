import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

### KONSTANTEN ###

BOLTZMANN = 1.380649e-23 # J/K = Nm/K
µ = 1e-6

R = 2.06 # µm
T = 25 + 273.15 # K

LASER = (930,117)
MICROMETERS_PER_PIXEL = 0.06105198218175463

#DATA = r'massstab/drift.hdf5'
#DATA = r'brownsche_bewegung/bb.hdf5'
#DATA = r'brownsche_bewegung/bb1.hdf5'
#DATA = r'brownsche_bewegung/bb2.hdf5'
DATA = r'Pinzette_funk/pinzette.hdf5'
#DATA = r'Pinzette_funk_brown/pinzette.hdf5'

##################

# Datei einlesen
dataframe = pd.read_hdf(DATA)
df = dataframe.set_index('particle')

#-- ZUM FINDEN DER GEFANGENEN PARTIKEL --#
# df = df[np.abs(df['x'] - LASER[0]) < 100]
# df = df[np.abs(df['y'] - LASER[1]) < 100]
# print(df)
# exit()
#--                                    --#

particle_data = df.loc[119] # frei: 119 | gefangen: 155
x = np.array(particle_data['x']) * MICROMETERS_PER_PIXEL
y = np.array(particle_data['y']) * MICROMETERS_PER_PIXEL

plt.title('Trajektorie')
plt.gca().invert_yaxis()
plt.plot(x-x[0], y-y[0], linewidth=1, color='black')
plt.xlabel('Verschiebung in x-Richtung [µm]')
plt.ylabel('Verschiebung in y-Richtung [µm]')
ax = plt.gca()
ax.set_aspect('equal', adjustable='box')
plt.show()



distance = np.sqrt(((x - x[0])**2 + (y - y[0])**2))
t = np.linspace(0, len(distance)/30, len(distance))

def func(x, a):
    return np.sqrt(a * x)

popt, pcov = curve_fit(func, t, distance)

plt.title('Verschiebung über Zeit')
plt.plot(t, distance)
#plt.plot(t, func(t, popt))
plt.ylabel('Verschiebung [µm]')
plt.xlabel('Zeit [s]')
plt.show()