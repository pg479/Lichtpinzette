import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit


### KONSTANTEN ###

µ = 1e-6

R = 2.06/2 # µm
NU = 0.0010157326319665924 # Ns/m²
RERR = 0.05 # µm
NUERR = 0.000105196049151901 # Ns/m²

##################



# Bestimmung der Haltekraft mit Stokesscher Reibung aus der maximalen Haltegeschwindigkeit
def fh(v): # v in µm/s -> F in fN
    return 6*np.pi* NU * R*µ * v*µ * 10**15

# Bestimmung des Fehlers der Haltekraft aus v und verr in µm/s
def fherr(v, verr):
    return np.sqrt(
        (6*np.pi* NUERR * R*µ * v*µ * 10**15)**2 +
        (6*np.pi* NU * RERR*µ * v*µ * 10**15)**2 +
        (6*np.pi* NU * R*µ * verr*µ * 10**15)**2
    )

# Lineare Fitfunktion durch Ursprung
def func(x, a):
    return a * x



### MESSWERTE ###

x = np.array([3.1, 2.0]) # I [mW]
y = np.array([0.9, 0.5]) # v [µm/s]
xerr = x*0.01 # mW
yerr = np.array([0.2,0.2]) # µm/s

#################



yerr = fherr(y, yerr) #fN
y = fh(y) # fN

print(y)
print(yerr)



### DIAGRAMM ###

plt.errorbar(x, y, yerr, xerr, fmt='k.', capsize=2)

popt, pcov = curve_fit(func, x, y)

sqrs = np.sum((func(x, popt[0]) - y)**2)
err = np.sqrt(len(x)*sqrs / (len(x)*np.sum(x**2) - np.sum(x)**2))

x_ = np.linspace(0,3.5)
plt.plot(x_, func(x_, popt),"r--", label=f'{round(popt[0],3)}fN/mW')

plt.xlim(0,3.5)
plt.ylim(0,25)
plt.xlabel('Laserintensität unter dem Objektiv [mW]')
plt.ylabel('Haltekraft [fN]')
plt.legend()
plt.grid()
plt.show()

##############